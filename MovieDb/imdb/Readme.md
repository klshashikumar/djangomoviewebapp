# Registering the Models to Administrator

## Step 1

Add below lines to admin.py

```python
from imdb.models import Actors, Movies

admin.site.register(Actors)
admin.site.register(Movies)

```

## Actor model
![alt text](https://c9771b46-a-62cb3a1a-s-sites.googlegroups.com/site/viruseducation/python/Actors.JPG)

## Movie model

![alt text](https://c9771b46-a-62cb3a1a-s-sites.googlegroups.com/site/viruseducation/python/Movies.JPG)



## Step 2

### Create a super User

```bash 

python manage.py createsuperuser

```


