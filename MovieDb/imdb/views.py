from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import TemplateView, ListView
from imdb.models import Actors, Movies

#Method to Add Docters
def addActors(request):
        if request.method == 'POST':
            if request.POST.get('name') and request.POST.get('sex'):
                obj=Actors()
                obj.name= request.POST.get('name')
                obj.sex= request.POST.get('sex')
                obj.dob= request.POST.get('dob')
                obj.bio = request.POST.get('bio')
                obj.save()
                print("obj Saved")
                return render(request,'AddActors.html')  
        else:
                return render(request,'AddActors.html')
                
#Method to Add Movie to Database
def addMovie(request):
        if request.method == 'POST':
            if request.POST.get('moviename') and request.POST.get('yearofrelease'):
                obj=Movies()
                obj.movie_name= request.POST.get('moviename')
                obj.year_of_release= request.POST.get('yearofrelease')
                obj.plot= request.POST.get('plot')
                obj.poster = request.POST.get('image')
                obj.cast = request.POST.getlist('cast')
                obj.save()
                print("obj Saved")
                item  = Movies.objects.all() # use filter() when you have sth to filter ;)
                form = request.POST 
                return render(request, 'MovieListing.html', {'items': item,}, ) 
        else:
                return render(request,'MovieListing.html')

def editMovie(request):
  if request.method == 'GET':
    movie_id = request.GET.get('values')
  return render(request,'editMovie.html',{'id':movie_id},)
        
#Method toSelect Actor name in Dropdown list
def selectview(request):
    item  = Actors.objects.all() 
    form = request.POST 
    if request.method == 'POST':
        selected_item = get_object_or_404(Item, pk=request.POST.get('name'))
  
        user.item = selected_item
        user.save()
    #passing only actor_name to AddMovie.html
    return render(request, 'AddMovie.html', {
        'items': item,
    }, )


#ListView of the all the Movies
def Movieview(request):
    item  = Movies.objects.all() 
    form = request.POST 
    return render(request, 'MovieListing.html', {'items': item,}, )



