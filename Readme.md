# Django web application

## Requirements

* python 3
* Django 1.11
* mysql / postgresql( I have used https://api.elephantsql.com)


## Creating a project using Django
```bash
django-admin startproject MovieDB
```
## Creating a app using Django
```bash
django-admin startapp MovieDB
```
or

```bash
python manage.py startapp MovieDB
```

## Screenshots
### Folder Structures

![alt text](https://c9771b46-a-62cb3a1a-s-sites.googlegroups.com/site/viruseducation/python/Capture.JPG)

### Movie listing

![alt text](https://c9771b46-a-62cb3a1a-s-sites.googlegroups.com/site/viruseducation/python/Movielisting.JPG)

### Movie and Actors adding

![alt text](https://c9771b46-a-62cb3a1a-s-sites.googlegroups.com/site/viruseducation/python/AddmovieANDactors.JPG)

### Edit on Click
![alt text](https://c9771b46-a-62cb3a1a-s-sites.googlegroups.com/site/viruseducation/python/Edit%20on%20Click.JPG)

## step 1
Open settings.py

Add app name to INSTALLED_APPS 

```python 

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'imdb',
]
```
 Add DB properties

```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'kwtsdlrh',
        'USER':'kwtsdlrh',
        'PASSWORD':'7agp8voa3v3srgR87yYdwkYrsgYeeYw4',
        'HOST':'raja.db.elephantsql.com',
        'PORT':'5432',
    }
}
```

Set the Media folder.     
Add below two lines 

```python

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'
```

## Step 2(URL setting)

### admin.urls
```python
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include('imdb.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

```
### app.urls

```python
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.Movieview, name='index'),
    url(r'addmovie$', views.addMovie, name='movieadd'),
    url(r'edit$', views.editMovie, name='edit'),
    url(r'home$', views.Movieview, name='select'),
    url(r'addactors$', views.addActors, name='list'),
    url(r'list$', views.Movieview, name='list'),

]
```

## Step3 (VIEWS)

```python
from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import TemplateView, ListView
from imdb.models import Actors, Movies
def addActors(request):
        if request.method == 'POST':
            if request.POST.get('name') and request.POST.get('sex'):
                obj=Actors()
                obj.name= request.POST.get('name')
                obj.sex= request.POST.get('sex')
                obj.dob= request.POST.get('dob')
                obj.bio = request.POST.get('bio')
                obj.save()
                print("obj Saved")
                return render(request,'AddActors.html')  
        else:
                return render(request,'AddActors.html')
def addMovie(request):
        if request.method == 'POST':
            if request.POST.get('moviename') and request.POST.get('yearofrelease'):
                obj=Movies()
                obj.movie_name= request.POST.get('moviename')
                obj.year_of_release= request.POST.get('yearofrelease')
                obj.plot= request.POST.get('plot')
                obj.poster = request.POST.get('image')
                obj.cast = request.POST.getlist('cast')
                obj.save()
                print("obj Saved")
                item  = Movies.objects.all() # use filter() when you have sth to filter ;)
                form = request.POST 
                return render(request, 'MovieListing.html', {'items': item,}, ) 
        else:
                return render(request,'MovieListing.html')
def editMovie(request):
  if request.method == 'GET':
    movie_id = request.GET.get('values')
  return render(request,'editMovie.html',{'id':movie_id},)
def selectview(request):
    item  = Actors.objects.all() 
    form = request.POST 
    if request.method == 'POST':
        selected_item = get_object_or_404(Item, pk=request.POST.get('name'))
        user.item = selected_item
        user.save()
    return render(request, 'AddMovie.html', {
        'items': item,
    }, )
def Movieview(request):
    item  = Movies.objects.all() 
    form = request.POST 
    return render(request, 'MovieListing.html', {'items': item,}, )
```

## Step 4 (MODEL)

```python
from django.db import models
class Actors(models.Model):
    Actor_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    sex = models.CharField(max_length=50)
    dob = models.DateField()
    bio = models.CharField(max_length=500)
    class Meta:
        db_table="Actors"

class Movies(models.Model):
    movie_id = models.AutoField(primary_key=True)
    movie_name = models.CharField(max_length=50)
    year_of_release = models.IntegerField()    
    plot = models.CharField(max_length=500)
    poster =  models.ImageField(blank=True, null=True, upload_to="images/")
    cast = models.CharField(max_length=800)
    class Meta:
        db_table="Movies"
```

### Make Migration
To migrate any changes in the DB
```bash 
python manage.py makemigrations
python manage.py migrate
```
## Step 5 (Templates)

Create HTML templates and store it in Template Folder

# Running the Application

```bash
python manage.py runserver
```


